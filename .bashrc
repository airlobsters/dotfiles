export PATH=~/anaconda3/bin/:$PATH
export GPG_TTY=$(tty)
export PS1="\t\[\033[01;93m\]\w\[\033[00m\]\$ "

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/root/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/root/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/root/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/root/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
eval "$(register-python-argcomplete3 conda)"
neofetch


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/repos/speech-sum-llm/google-cloud-sdk/path.bash.inc' ]; then . '/repos/speech-sum-llm/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/repos/speech-sum-llm/google-cloud-sdk/completion.bash.inc' ]; then . '/repos/speech-sum-llm/google-cloud-sdk/completion.bash.inc'; fi
